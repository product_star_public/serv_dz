import json
from flask import Flask, jsonify, request

from model.post import Post
from model.comment import Comment

posts = []
comments = []

app = Flask(__name__)


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Post):
            return {'author': obj.author, 'body': obj.description}
        return super().default(obj)


app.json_encoder = CustomJSONEncoder


@app.route("/ping", methods=["GET"])
def ping():
    return jsonify({"response": "pong"})


@app.route("/post/<id>", methods=["GET"])
def read_post(id: int):
    for item in posts:
        if item["id"] == int(id):
            return jsonify({"post": item})
    return jsonify({"status": "error"}), 400


@app.route("/post", methods=["POST"])
def create_post():
    post_json = request.get_json()
    post = Post(post_json["author"], post_json["description"])
    try:
        post_json.update({"id": posts[-1]["id"] + 1})
    except IndexError:
        post_json.update({"id": 1})
    post_json.update({"comment": []})
    posts.append(post_json)
    return jsonify({"status": "success"})


@app.route("/post", methods=["GET"])
def read_posts():
    return jsonify({"posts": posts})


@app.route("/post/<id>", methods=["DELETE"])
def delete_post(id: int):
    for item in posts:
        if item["id"] == int(id):
            posts.remove(item)
            return jsonify({"status": "success"})

    return jsonify({"status": "error"}), 400


@app.route("/post/<id>", methods=["PUT"])
def put_post(id:int):
    post_json = request.get_json()
    for item in posts:
        if item["id"] == int(id):
            item["description"] = post_json["description"]
            item["author"] = post_json["author"]
            return jsonify({"status": "success"})
    return jsonify({"status": "error"})


@app.route("/comment", methods=["POST"])
def create_comment():
    comment_json = request.get_json()
    comment = Comment(comment_json["comment"], comment_json["author"], comment_json["post_id"])
    if not posts:
        return jsonify({"status": "error(no post)"})
    else:
        try:
            comment_json.update({"id": comments[-1]["id"] + 1})
        except IndexError:
            comment_json.update({"id": 1})
        comments.append(comment_json)
        for item in posts:
            if item["id"] == int(comment_json["post_id"]):
                item["comment"].append(comment_json)
        return jsonify({"status": "success"})


@app.route("/comment/<id>", methods=["DELETE"])
def read_comment(id: int):
    for item in comments:
        if item["id"] == int(id):
            comments.remove(item)
    for item in posts:
        for item2 in item["comment"]:
            if item2["id"] == int(id):
                item["comment"].remove(item2)
                return jsonify({"status": "success"})
    return jsonify({"status": "error"}), 400




if __name__ == "__main__":
    app.run(debug=True)
