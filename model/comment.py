from model.user import User
class Comment:
    def __init__(self, body_comment: str, author: User, post_id: int):
        self.comment = body_comment
        self.author = author
        self.post_id = post_id
